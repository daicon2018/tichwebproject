import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireDatabase, AngularFireList } from 'node_modules/firebase/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  
  
})
export class AppComponent{
  title = 'TICHWEB';
  
  constructor(router:Router) {
    router.navigate(['/construction']);
  }

}

interface User {
    name  : string;
    lastName : string;
    state : string;
}

