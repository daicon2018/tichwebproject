import { RestService } from './rest.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaestrosComponent } from './maestros/maestros.component';

import { RouterModule, Routes } from '@angular/router';
import { ConstructionComponent } from './construction/construction.component';


import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { TichfsService } from './services/tichfs.service';
import { MateriasComponent } from './materias/materias.component';
import { PreregistroComponent } from './preregistro/preregistro.component';


const appRoutes: Routes = [
  { path: 'home', component: AppComponent },
  { path: 'construction', component: ConstructionComponent},
  { path: 'maestros', component: MaestrosComponent },
  { path: 'materias', component: MateriasComponent}, 
  { path: 'preregistro', component: PreregistroComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    MaestrosComponent, 
    ConstructionComponent, 
    MateriasComponent, PreregistroComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase, 'angularfs'),
    AngularFirestoreModule,
    HttpClientModule
  ],
  providers: [RestService],
  bootstrap: [AppComponent]
})

export class AppModule { }
