import { Usuarios, RestService } from './../rest.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-construction',
  templateUrl: './construction.component.html',
  styleUrls: ['./construction.component.css']
})

export class ConstructionComponent implements OnInit {

  usuarios : Usuarios[];

  constructor( private restService : RestService) { }

  ngOnInit() {}

  add(nombre : string, email:string, apaterno:string, fechanac:string): void {
    let usuario : Usuarios = {
      nombre : nombre.trim(),
      email : email.trim(), 
      apaterno : apaterno.trim(),
      fechanac : fechanac.trim()
    };
    this.restService.addUser(usuario).subscribe(response =>{
      this.usuarios.push(usuario);
    })
  }

}
