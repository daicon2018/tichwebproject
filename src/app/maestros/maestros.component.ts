import { Component, OnInit } from '@angular/core';
import { TichfsService } from '../services/tichfs.service';
import { Item } from '../models/items';

@Component({
  selector: 'app-maestros',
  templateUrl: './maestros.component.html',
  styleUrls: ['./maestros.component.css']
})
export class MaestrosComponent implements OnInit {
  items: Item[];
  constructor(private itemService : TichfsService) { }

  ngOnInit() {
    console.log('ngOnInit ran');
    
    this.itemService.getItems().subscribe(items => { 
      console.log(items)
      this.items = items;
    });
  }

  showMaterias(){
    
  }

}
