
export const materias = [

        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Fundamentos Basicos de Admon."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Planeacion y Control"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Teoria de la Administracion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Contabilidad Financiera I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Contabilidad de Costos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Contabilidad Administrativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Matematicas Basicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Estadistica Descriptiva"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Intr. al Estudio del Derecho"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Derecho Mercantil I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Calculos Financieros"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Informatica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Fund. de Teoria Economica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Ingles Comunicativo (Princ.)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Recursos Humanos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Mercadotecnia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Investigacion de Mercados"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Principales of Operations Management"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Value and Supply Chain Management"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Organizacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Liderazgo y Direccion de Equipo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Estadistica Inferencial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Analisis Cuantitativo de los Negocios"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Derecho Laboral"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Finanzas I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Finanzas II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Taller de Ingles para negocios"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Ingles comunicativo (Intermedio)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Ingles comunicativo (Avanzado)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Competencias para el Desarrollo Hum. Sust."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Administracion de la Calidad"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Valores en el Ejercicio Profesional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Formacion y Evaluacion de Proyectos de Inv."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Auditoria Administrativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Estrategias del Cambio Organizacional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Creacion y Desarrollo de Empresas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Seminario de Investigacion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Administracion de empresas",
          "Materia": "Seminario de Investigacion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Mercadotecnia?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Introduccion Al Estudio del Turismo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Contabilidad Financiera I?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Fundamentos Basicos de la Administracion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Operacion y Gestion de Restaurantes"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Operacion de Hoteles"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Servicios de Viajes"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Operacion y Gestion de Bares"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Gestion Hotelera"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Tecnicas de Investigacion Documental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Tecnicas de Investigacion Documental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Fundamentos de Teoria Economica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Ingles Comunicativo (Principiante)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Estadistica Descriptiva"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Recursos Humanos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Investigacion de Mercados"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Creacion y Desarrollo de Empresas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Turismo y Medio Ambiente"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Mercadotecnia de Empre?sas Turisticas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Relaciones Publicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Administracion Estrategica de Empresas Turisticas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Costos en Restaurantes y Bares"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Organizacion de Congresos y Convenciones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Comportamiento Organizacional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Competencias Para El Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Ingles Comunicativo (Intermedio)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Seminario de Investigacion Turistica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Planificacion Turistica??a (En Ingles)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Seminario de Investigacion Turistica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Formulacion y Evaluacion de Proyectos de Inversion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Competencias Para El Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en turismo",
          "Materia": "Ingles Comunicativo (Avanzado)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Sistema Financiero Mexicano"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Matematicas Aplic. a las Finanzas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Introduccion a las Finanzas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Ingles Comunicativo Basico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Int. al Estudio del Derecho"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Contabilidad Financiera I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Informatica Financiera"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Contabilidad Financiera II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Fund. De Teo. Economica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Microeconomia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Estadistica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Ingles Comunicativo Intermedio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Derecho Mercantil I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Fundamentos Basicos de Admon."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Microeconomia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Comp. Para el Desarrollo Humano"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Ingles Comunicativo Avanzado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Macroeconomia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Derecho Administrativo I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Finanzas Corporativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Mercados Financieros"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Contabilidad de Costos II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Finanzas I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Finanzas II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Derecho Corporativo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Sistema financiero Internacional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Estadistica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Contabilidad de Costos I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Econometria Financiera"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Form. y Eval. De Proy. Inv."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Comp. Para el ejercicio da la ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Derivados I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Finanzas Internacionales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Macroeconomia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Derivados II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Administracion de riesgos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Seminario de Investigacion� I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en finanzas",
          "Materia": "Seminario de Investigacion� II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Tecnicas de Investigacion Documental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Cultura y Sociedad Mexicana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Lectura y Redaccion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Psicologia General I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Procesos Psicologicos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Teoria del Aprendizaje"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Psicopedagogia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Orientacion Vocacional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Pedagogia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Pedagogia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Corrientes Pedagogicas Contemporaneas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Didactica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Didactica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Elaboracion y Uso de Recursos Didacticos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Historia de la Educacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Epistemologia de la Educacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Estadistica Descriptiva"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Sociologia de la Educacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Fundamentos de Educacion Especial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Filosofia de la Educacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Metodos Electronicos y Ambientes de Aprendizaje"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Dise�o y Evaluacion Curricular"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Planeacion y Evaluacion de la Capacitacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Didactica de la Educ?acion Superior"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Investigacion Educativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Seminario de Investigacion Educativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Analisis Institucional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Educacion Comunitaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Analisis de la Educacion en Mexico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Gestion Escolar"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Evaluacion Educativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Planeacion Educativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Taller de Investigacion Educativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en educacion",
          "Materia": "Economia de la Educacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Introduccion al Estudio de la Historia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Taller de Lectura y Redaccion de Textos Historicos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Introduccion a la Historia Antigua y Clasica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historia de Culturas Antiguas Hemisferio Norte de America"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Taller de Recursos Informaticos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Geografia y Cartografia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Tecnicas de Investigacion Historica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historia del Mundo Iberico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Mexico Colonial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Competencias��Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historiografia Mexicana hasta el Siglo XIX"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historia Mundial Moderna y Contemporanea"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Fundamentos Teoricos de la Historia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Filosofia de la Historia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Formacion Historica del Estado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Teoria de la Historia 1"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historia Regional I: Formacion del norte de Mexico 1848 a la actualidad."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historia del siglo XIX en Mexico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historia Mundial, Moderna y Contemporanea"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Servicio Social"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Teoria de la Historia 2"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historia regional II: Frontera Norte de Mexico 1848 a la actualidad."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historia latinoamericana moderna y contemporanea"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Seminario de Proyectos de investigacion 1:�El Horizonte Multidisciplinario"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Sistemas para la Difusion de la Historia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Taller de Historia Oral"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Desarrollo y Estrategias de Aprendizaje"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Archivistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Competencias para el Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Mexico en el Siglo XX"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historiografia Mexicana Moderna"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Taller de Paleografia y Diplomatica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Didactica de la Historia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en historia",
          "Materia": "Historia de Estados Unidos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Introduccion a la teoria literaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Literatura medieval espa�ola"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Literatura prehispanica y novohispana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Mitologia y cultura clasica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Morfosintaxis del espa�ol I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Tecnicas de investigacion literaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Teoria literaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Literatura de los Siglos de Oro I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Literatura novohispana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Mitologia y cultura clasica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Morfosintaxis del espa�ol II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Introduccion al analisis literario: metrica y retorica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Historia de la critica literaria I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Literatura de los Siglos de Oro II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Teoria y analisis del texto poetico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Ling�istica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Latin I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Historia de la critica literaria II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Neoclasicismo y Romanticismo en Espa�a"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Ling�istica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Latin II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Analisis literario I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Del Realismo al Novecentismo en Espa�a"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Del Realismo a la novela de la Revolucion en Mexico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Latin III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Analisis literario II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Neoclasicismo y Romanticismo en Mexico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Poesia mexicana moderna"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Latin IV"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Generacion del 27 y vanguardias"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Teoria y analisis del teatro"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Teoria y analisis del texto narrativo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Narrativa y ensayo mexicanos contemporaneos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Literatura latinoamericana I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Literatura espa�ola de la Guerra Civil a nuestros dias"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "De la formacion del teatro nacional mexicano a nuestros dias"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Literatura latinoamericana II: El boom"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Taller de investigacion literaria I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Literatura Hispano mexicana",
          "Materia": "Taller de investigacion literaria II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Redaccion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Introduccion a la Comunicacion de Masas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Logica?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Tecnicas de Investigacion Document?al"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Estadistica Aplicada a las Ciencias Sociales?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Redaccion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Introduccion al Periodismo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Temas selectos de Psicologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Temas selectos de Sociologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Geopolitica�y Globalizacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Periodismo Escrito I (generos informativos)���"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "??Espa�ol Avanzado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Fotografia Periodistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Literatura Contemporanea"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Computacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Periodismo Escrito II (generos opinativos)��"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Periodismo Radiofonico I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Periodismo Televisivo I?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Comunicacion Oral"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Economia Regional?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Periodismo Escrito III (generos hibridos)�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Periodismo Radiofonico II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Periodismo Televisivo II?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Codigos Deontologicos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Historia Contemporanea de Mexico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Sistema Politico Mexicano"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Problemas Fronterizos Contemporaneos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Ecologia de la Frontera?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Marco legal de la Comunicacion Fronteriza?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Historia general de Estados Unidos (en ingles)?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "Taller de Entrevista"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Taller de Periodismo Politico�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Taller de peque�as Publicaciones�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "??Taller de Periodismo en la Red?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Periodismo",
          "Materia": "?Historia de los Movimientos Migratorios Mexico-EU"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Introduccion al Estudio de Derecho (JUR1000)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Logica Juridica (JUR1006)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Fundamentos de Teoria Economica (ECO1000)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Teoria General del Derecho (JUR1002)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Contabilidad� Financiera (CIA3000)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Principios Basicos de Administracion (CIA1000)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Mercadotecnia (CIA1207)��"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Civil I (JUR1010)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Civil II (JUR1200)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Teoria General del Proceso (JUR1201)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Teoria General del Estado (JUR1008)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Penal I (JUR1009)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Nomenclatura I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Competencias Comunicativas�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Introduccion al Comercio Exterior"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Regulaciones y Restricciones no Arancelarias��"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Constitucional (JUR1202)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Civil III (JUR1206)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Terminos Comerciales Internacionales (Intercoms)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Nomenclatura II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Garantias del Gobernado (JUR1401)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Civil IV (JUR1213)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Administrativo (JUR1210)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Competencias para el desarrollo humano sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Aduanero (JUR1627)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Valor en Aduana de las Mercancias y Precios de Transferencia�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho de la Integracion Economica�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Reglas del Comercio Exterior en materia de Comercio Exterior"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Fiscal (JUR1405)��"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Despacho Aduanero"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Impuestos al Comercio Exterior e Impuestos Internos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Internacional Publico (JUR1400)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Reglas de Origen y Procedimientos Aduaneros"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Procesal Fiscal y Aduanero (JUR1617)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Compra Venta Internacional de Mercaderias�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Competencias del Ejercicio de Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Tratado Libre Comercio de America del Norte"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Programas e Instrumentos de Fomento al Comercio Exterior"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Despacho Aduanero"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Infracciones y Delitos Aduaneros�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Auditoria en Comercio Exterior�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Tratados y Acuerdos de Libre Comercio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "etica del Ejercicio Profesional (JUR1642)�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Conflictual"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Legislacion sobre Inversion Extranjera"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Derecho Comparado�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Metodos de Solucion de Conflictos en Materia de Comercio Exterior."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Logistica y Operaciones de Transporte Internacional�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Medios de Impugnacion en Materia Fiscal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Competencia Economica �"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Comercio Exterior",
          "Materia": "Organizacion Mundial de Comercio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Tecnicas de Investigacion ?Documental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Lectura y Redaccion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Introduccion Al Estudio del Derecho"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Historia del Derecho I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Teoria General del Derecho"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Historia del Derecho II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Seminario de Cultura Juridica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Logica Juridica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho de Familia??"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Teoria General del Estado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Penal I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Civil I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Metodologia del Derecho"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Civil II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Teoria General del Proceso"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Sociologia del Derecho"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Constitucional I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Penal II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Mercantil I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Constitucional II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Civil III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Procesal Penal?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Mercantil II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Der.Proc. Civ. Merc. y Fam. I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Administrativo I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Laboral"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Civil IV"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Procesal Laboral"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Garantias del Gobernado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Corporativo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Argumentacion Juridica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Der.Proc.Civ.Mer. y Fam. II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Internacional Publico?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Filosofia del Derecho"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Conflictual"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho Fiscal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "Derecho de Amparo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Derecho",
          "Materia": "etica del Ejercicio Profesional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Contabilidad Financiera?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Cultura y Sociedad Mexicana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Lectura y Redaccion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Fundamentos de Teoria Economica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Historia del Pensamie??nto Economico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Microeconomia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Microeconomia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Macroeconomia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Macroeconomia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Economia Politica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Geografia Economica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Moneda Banca y Mercados Financieros"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Crecimiento Economico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Matematicas Basicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Estadistica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Matematicas Aplicadas a la Economia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Estadistica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Matematicas Financieras"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Economia de la Empresa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Finanzas Publicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Economia Industrial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Formulacion y Evaluacion de Proyectos de Inversion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Economia Regional y Urbana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Economia Internacional?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Econometria I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Econometria II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Economia Ambiental y de los Recursos Naturales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Problemas Economicos y Sit. Actual de Mexico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Economia del Sector Agropecuario"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Desarrollo Economico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Metodologia de la Investigacion Economica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Taller� de Asesoria y Consultoria Empresarial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Seminario de Economia Fronteriza"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Taller de Investigaci?on Economica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Taller de Investigacion Economica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Seminario de Pol. Econ. Mexicana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Economia",
          "Materia": "Seminario de Temas Economicos Contemporaneos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicologia General?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Neuroanatomia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Introduccion la Teoria del Conocimiento."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Bases Biologicas de la Conducta�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Ingles Comunicativo I�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Medicion en Psicologia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Sensopercepcion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Cultura y Sociedad Mex?icana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Motivacion y Emocion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Ingles Comunicativo II�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Medicion en Psicologia II��������������������������������������������������������"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Desarrollo Psicologico�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Corrientes Contemporaneas de la Psicologia�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Procesos del Aprendizaje"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Ingles Comunicativo III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Pensamiento y Lenguaje"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Medicion en Psicologia III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "etica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicologia Social"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Competencias en Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Pensamiento Critico y Creativo�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicometria�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicopatologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Entrevista Psicologica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicologia Experimental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Metodologia de las Ciencias ?Sociales�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicologia Clinica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicologia Educativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicologia Organizacional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicologia Transcultural"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Topicos de Psicologia I�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Topicos de Psicologia II�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicoterapia�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Competencias en C??iudadania y Democracia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Modificacion de Conducta"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Seminario de Investigacion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Psicologia Comunitaria�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Psicologia",
          "Materia": "Seminario de Investigacion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Metodos de Intervencion Social?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Dinamicas y Tecnicas de Grupo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Elaboracion de Proyectos Sociales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Introduccion al Trabajo Social?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Elaboracion de Documentos Academicos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Epistemologia y Metodologia de las Ciencias Sociales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Teoria y Modelos del Desarrollo Humano y Social"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Trabajo Social Institucional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Tecnicas y Recoleccion de Datos Cualitativos y Cuantitativos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "etica�del Trabajo Social"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Epistemologia del Trabajo Social"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Intervencion de Trabajo Social en Familia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "T. de Analisis de la Real. Pol. y Eco. de Mex."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Metodologia y Practica de Trabajo Social de Grupo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Analisis Cuantitativa para la Intervencion Social?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Problemas de Desarrollo Regional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Metodologia y Practica de Trabajo Social de Comunidad I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Analisis Cualitativos para la Intervencion Social"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Trabajo Social en el Campo Juridico II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Trabajo Social en Salud Publica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Metodologia y Practica de Trabajo Social De Comunidad II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Trabajo Social Empresarial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Trabajo Social En El Campo Juridico I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Competencias para el Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Politica y Planificacion Soc."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Seminario de Investigacion del Trabajo Social"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Taller de Practica Institucional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Gerencia Social y Trabajo Social??"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Sistematizacion en el Trabajo Social"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Equidad de Genero y Mod. Interv. T. Soc."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Trabajo Social Y Derechos Humanos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Metodologia y Practicas de Trabajo Social Individual Y Familiar"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Trabajo Social en el ambito Educativo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Trabajo Social",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Materia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Fisica Conceptual"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Calculo I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "algebra"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Introduccion al Laboratorio de Fisica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Dibujo Industrial Asistido por Computadora"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Competencias Comunicativas con Enfoque de Genero"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Fisica General I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Calculo II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Quimica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Probabilidad y Estadistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Laboratorio de Fisica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Desarrollo De La Creatividad e Innovacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Fisica General II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Calculo Multivariable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Matrices Y Transformaciones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Termodinamica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Laboratorio De Fisica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Formacion Integral En Arte Y Cultura"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Fisica General III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Calculo Vectorial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Ecuaciones Diferenciales Ordinaria I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Electrometria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Competencias Para El Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Ingles Comunicativo Principiante"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "optica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Variable Compleja"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Metodos Matematicos de la Fisica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Aplicaciones Computacionales I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Circuitos Electricos I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Ingles Comunicativo Intermedio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Mecanica Clasica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Electrodinamica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Metodos Matematicos de la Fisica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Aplicaciones Computacionales II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Electronica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Ingles Comunicativo Avanzado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Introduccion a la Fisica de Materiales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Fisica Moderna"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Fisica Aplicada"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Sistemas de Adquisicion De Datos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Procesos De Manufactura I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Mecanica Cuantica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Fisica Estadistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Fisica de Medios Continuos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Proyecto de Titulacion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Fisica del Estado Solido"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Proyecto de Titulacion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Fisica",
          "Materia": "Desarrollo Empresarial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Calculo I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Fisica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Quimica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "algebra"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Fundamentos de Programcion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Introduccion a las Tecnologias de Informacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Calculo II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Fisica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Probabilidad y Estadistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Programacion Estructurada"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Interaccion Humano Computadora"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Calculo III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Fisica III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Ecuaciones Diferenciales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Analisis Numerico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Programacion Orientada a Objetos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Contabilidad y Costos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Teoria Electromagnetica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Sistemas Lineales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Sistemas Web I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Estructura de Datos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Ingles Comunicativo Principiante"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Competencias para el Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Inteligencia Artificial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Teoria de la Computacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Bases de Datos I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Analisis y Dise�o de Algoritmos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Ingles Comunicativo Intermedio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Modelos Economicos para la Toma de Decisiones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Redes de Computadoras I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Circuitos Digitales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Ingenieria de Requisitos, Analisis y Dise�o de Sistemas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Sistemas Operativos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Ingles Comunicativo Avanzado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Desarrollo Empresarial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Redes de Computadoras II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Arquitectura de Computadoras I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Ingenieria de Software"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Seguridad en Computo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Seminario de Titulacion de Sistemas Computacionales I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Programacion Integrativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Administracion de la Funcion de Tecnologias de Informacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Simulacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria en sistemas Computacionales",
          "Materia": "Seminario de Titulacion de Sistemas Computacionales II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Anatomia H??humana I?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Anatomia Humana II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Fisiologia Humana I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Bioquimica General"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Microbiologia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Microbiologia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Embriologia Humana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Histologia General"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Bioquimica Medica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Fisiologia Humana II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Salud Comunitaria I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Genetica Medica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Taller de Integracion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Farmacologia Medica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Anatomia por Imagen"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Nomenclatura Medica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "etica y Filosofia de la Medicina"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Metodologia de la Investigacion en Salud"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Fisicoquimica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Histopatologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Biologia Celular y Molecular"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Tic�S� Aplicadas a las Ciencias de la Salud"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Estadistica Descriptiva"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Competencias para el Desarrollo Humano Sustent?able"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Medicina Basada en Evidencias"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Psicologia Medica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Sexualidad Humana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Inmunologia Medica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Habilidades Medico Quirurgicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Cirugia General"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Endocrinologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Propedeutica Medica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Investigacion Clinica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Terapeutica Medica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Salud Comunitaria II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Reumatologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Cardiologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Ortopedia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Urologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Clinica del Dolor"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Psiquiatria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Hematologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Estadistica Inferencial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Epidemiologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Internado Clinico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Infectologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Gastroenterologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Nutricion Medica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Oftalmologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Otorrinolaringologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Neumologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Dermatologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Nefrologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Geriatria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Medicina Comunitaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Pediatria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Ginecobstetricia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Topicos Selectos de Medicina Laboral y Legal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Taller de Integracion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Medicina en Urgencias"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Investigacion Clinica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Medico",
          "Materia": "Neurologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Matematicas Aplicadas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Biofisica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Fundamentos Moleculares"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Biomoleculas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Fisicoquimica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Ciencias de la Tierra y de la Atmosfera"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Citologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Metodologia de la Investigacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Competencias Comunicativas con Enfoque de Genero"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Analisis Instrumental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Ingles Comunicativo (Principiante)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Biologia de Campo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Ecologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Bioquimica�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Competencias Para El Desarrollo Humano Sustentable Con Enfoque��De Genero�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Morfo fisiologia Vegetal�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Biologia de Cordados"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Arquegoniadas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Espermatofitas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Biologia de Invertebrados"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Artropodos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Monera"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Eumicota"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Protoctistas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Investigacion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Estadistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Dise�o Experimental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Ingles Comunicativo (Intermedio)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Biologia del Desarrollo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Competencias para el Ejercicio de la Ciudadania con Enfoque de Genero"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Biologia Molecular"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Ecologia Cuantitativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Genetica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Biologia de la Conservacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Biogeografia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Paleo biologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Sistematica y Taxonomia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Investigacion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Investigacion III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Liderazgo y Emprendedurismo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Sistemas de Informacion Geografica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Impacto Ambiental y Analisis de Riesgo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Tecnicas de Biologia Molecular y Genetica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Ingles Comunicativo (Avanzado)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Evolucion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Biologia",
          "Materia": "Genetica de Poblaciones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Quimica General?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Quimica Inorganica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Quimica Organica I??"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Matematicas I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Quimica Organica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Biologia Celular"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Metodologia de la Investigacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Estadistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Matematicas II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Analisis Quimico I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Fisicoquimica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Fisicoquimica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Algebra Lineal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Ingles Comunicativo (Principiante)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Ingles Comunicativo (Intermedio)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Fisica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Nivel Intermedio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Analisis Quimico II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Microbiologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Investigacion I??"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Quimica Computacional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Bioquimica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Anatomia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Bioquimica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Inmunologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Bioquimica Clinica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Microbiologia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Farmacia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Farmacia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Fisiologia General"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Micologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Epidemiologia General"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Microbiologia III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Competencias Para El Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Ingles Comunicativo (Avanzado)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Nivel Avanzado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Quimica Organica III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Dise�o Experimental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Investigacion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Investigacion III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Inmunoquimica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Hematologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Analisis Quimicos III?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Biologia Molecular"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Patologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Control De Calidad"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Farmacologia Clinica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Fisicoquimica Farmaceutica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Tecnologia Farmaceutica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Desarrollo Farmaceutico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Tecnologia Farmaceutica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Farmacia III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Patologia General con Laboratorio?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Anatomia General"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Anatomia Cabeza y Cuello"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Fisiologia Odontol??logica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Anatomia Dental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Bioquimica Basica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Histologia Celular"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Fisiologia Celular"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Histologia y Embriologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Microbiologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Materiales Dentales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Odontologia Preventiva I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Odontologia Preventiva II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Oclusion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Radiologia Buco-Dental I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Patologia Buco-Dental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Farmacologia Odontologica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Propedeutica Odontologica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Prostodoncia Total I?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Pre-Operatoria Dental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Radiologia Buco-Dental II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Anestesia Buco-Dental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Tecnicas Quirurgicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Prostodoncia Total II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Protesis Parcial, Fija y Removible I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Operatoria Dental I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Endodoncia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Periodoncia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Exodoncia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Prostodoncia Total III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Protesis Parcial, Fija y Removible II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Operatoria Dental II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Endodoncia II?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Periodoncia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Exodoncia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Protesis Parcial, Fija y Removible III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Operatoria Dental III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Odontalgia Infantil"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Cirugia Bucal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Clinica Integral I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Clinica Infantil"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Cirujano Dentista",
          "Materia": "Clinica Integral II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Introduccion a la Teoria E Hist. de la Arq."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Geometria Descriptiva Tridimensional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Introduccion al Dise�o Arquitectonico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Representacion Arquitectonica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Espacio y Estructura"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Historia Critica de la Arquitectura II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Arquitectura Mexicana y Regional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Arquitectura II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Introduccion a la Construccion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Teorias de la Arquitectura"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Introduccion al Proyecto Arquitectonico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Estructuras Arquitectonicas I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Instrumentos de Rep. por Computadora I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Instrumentos de Representacion de Maquetas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Historia Critica de la Arquitectura I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Arquitectura I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Narrativa Arquitectonica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller De Estructuras Arquitec?tonicas II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Instrumentos de Rep. por Computadora II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Historia del Arte"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Dibujo Natural"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Arquitectura y Medio Ambiente"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Teorias Contemporaneas de la Arquitectura"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Arquitectura III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Arquitectura IV"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Arquitectura V"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Sem. de Investigacion en Arquitectura II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Sem. de Investigacion en Arquitectura I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Sem. de Investigac??ion en Arquitectura III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Sem de Investigacion en Arquitectura IV"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Puentes Interdisciplinarios"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Construccion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Construccion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Construccion III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Arquitectura VI"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Teorias Urbanas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Teorias de la Vivienda"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Proyecto de Titulacion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Taller de Proyecto de Titulacion ?II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Investigacion en la Arquitectura"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Seminario de Temas de Titulacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Temas Selectos de Investigacion Arquitectonica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Administracion de Arquitectura I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Administracion de Arquitectura II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Arquitectura",
          "Materia": "Gestion y Servicios Profesionales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Matematicas para el Dise�o Indus?trial I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Matematicas para el Dise�o Industrial II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Introduccion al Dise�o Industrial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Taller de Bocetaje de Producto"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Taller de Modelos y Moldes"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Geometria Descriptiva y Perspectiva de Producto"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Dise�o Basico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Introduccion a los Tallere??s de Manufactura"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Conceptualizacion y Creatividad"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Metodologia para la Investigacion del Dise�o"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Analisis Historico� de los Objetos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Taller de Expresion de Producto"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Ergonomia Aplicada Al Dise�o Industrial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Dibujo Tecnico Mecanico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Laboratorio de Cad"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Procesos en Madera y Metal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Eco Dise�o"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Idioma I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Taller de Metodo de Dise�o Industrial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Dise�o de Mobiliario"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Ilustracion Digital del Producto"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Laboratorio de Antropometria y Biomecanica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Electricidad y Electronica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Dise�o y Fabricacion de Moldes Plasticos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Estructuras y Resistencia de Materiales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Procesos en Ceramica y Vidrio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Fotografia Digital del Producto"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Mecanismos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Dise�o de Espacios e Iluminacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Dise�o de Envase y Embalaje"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Competencias para el Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Manufactura y Produccion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Sistemas de Calidad"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Gestion del Dise�o"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Publicidad y Mercadotecnia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Presentacion Digital de Proyectos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Dise�o de Movilidad Urbana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Planeacion y Dise�o de Producto"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Laboratorio de Calculo de Embalaje"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Administracion y Costos de Proyectos del Dise�o Industrial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Implementacion y Validacion de Producto"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Legislacion del Dise�o"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Industrial",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Fundamentos del Dise�o"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Tecnologia para el dise�o I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "fundamentos del Dibujo?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Bocetaje e Ilustracion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Expresion Plastica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Analisis de la Cultura y el Arte"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Historia del Dise�o Grafico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Dise�o Grafico en Mexico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Metodologia del Dise�o"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Tecnologia para el dise�o II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Geometria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Introduccion a la imagen"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Percepcion y el Color"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Matematicas para el Dise�o Grafico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Teorias de la Comunicacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Introduccion a la Teoria del Dise�o"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Dise�o Tipografico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Identidad y Sistemas Visuales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Infodise�o"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Competencias para el Desarrollo Humano"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Idioma I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Introduccion a la Mercadotecnia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Pre-prensa Digital"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Introduccion a la publicidad?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Fotografia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Laboratorio de Ergonomia para el Dise�o Grafico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Dise�o Editorial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Computacion Grafica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Principios de Investigacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Animacion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Idioma II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Taller de Semiotica y Dise�o"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Taller de Produccion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Taller de Produccion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Fotografia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Video Digital"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Marca y Envase"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Dise�o Multimedia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Seminario de�investigacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Animacion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Competencias en el Ejercicio de la Ciudadania??"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Taller de Retorica y Semiotica de la Imagen"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Taller de Sintesis de Dise�o Grafico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Taller Integral de Dise�o Grafico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Formacion Empresarial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Procesos y Costos en el Dise�o"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Dise�o Grafico",
          "Materia": "Taller de Discurso para el Dise�o Grafico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Calculo I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Fisica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Quimica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Contabilidad y Costos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "algebra"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Fundamentos de Programacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Calculo II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Fisica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Dibujo Industrial Asistido por Computadora"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Fisica III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Probabilidad y Estadistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Bono Deportivo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Bono Cultural"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Calculo III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ecuaciones Diferenciales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Analisis Numerico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ingenieria Electrica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Metodos Inferenciales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Competencias Comunicativas con Enfoque de Genero"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ingenieria de Metodos I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Seguridad Industrial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Competencias para el Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Control Estadistico de Calidad"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Dise�o de Experimentos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ingles Comunicativo Principiante"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ingenieria de Metodos II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Toma de Decisiones Multicriterio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Simulacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ingenieria Economica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Investigacion de Operaciones I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ingles Comunicativo Intermedio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Planeacion y Dise�o de Instalaciones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ingenieria de Procesos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ergonomia Industrial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Evaluacion de Proyectos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Investigacion de Operaciones II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ingles Comunicativo Avanzado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Sistemas de Produccion Esbeltos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Seminario de Administracion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Sistemas de Planeacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Ingenieria de Calidad"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Relaciones Industriales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Produccion mas Limpia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Administracion de Proyectos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Sistema para el Control de la Produccion y los Inventarios"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Seminario de Titulacion Industrial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Desarrollo Empresarial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Industrial y de Sistemas",
          "Materia": "Administracion de la Cadena de Suministro"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Calculo I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Geometria Analitica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Razonamiento Matematico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Competencias Comunicativas con Enfoque de Genero"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Fisica Conceptual�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Calculo II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Matrices y Transformaciones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Temas Selectos de Matematicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Lenguaje Matematico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Fisica General I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Calculo Multivariable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "algebra Lineal I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Programacion y Algoritmos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Introduccion al Analisis"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Historia de las Matematicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Calculo Vectorial�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "algebra Lineal II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Analisis Matematico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Variable Compleja�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Didactica de las Matematicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Probabilidad"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "algebra Moderna I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Topologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Ecuaciones Diferenciales Ordinarias I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Matematicas",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Calculo I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Fisica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Quimica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "algebra"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Fundamentos de Programacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Calculo II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Fisica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Probabilidad y Estadistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Dibujo Asistido por Computadora"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Programacion Estructurada"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Ecuaciones Diferenciales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Calculo III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Fisica III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Analisis Numerico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Electrometria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Sistemas Lineales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Circuitos Electricos I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Teoria Electromagnetica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Circuitos Digitales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Ingles Comunicativo Principiante"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Electronica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Circuitos Electricos II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Conversion de la Energia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Contabilidad y Costos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Ingles Comunicativo Intermedio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Competencias para el Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Electronica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Conversion de la Energia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Sistemas Digitales II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Modelos Economicos para la Toma de Decisiones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Ingles Comunicativo Avanzado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Alumbrado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Desarrollo Empresarial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Teoria de Control"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Redes Distribucion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Introduccion a las Energias Renovables"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Instalaciones Electricas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Mantenimiento de Instalaciones Industriales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Electronica de Potencia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Sistemas Electricos de Potencia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Uso Eficiente de la Energia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Seminario de Titulacion de Ingenieria Electrica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Proyecto Integrador de Ingenieria Electrica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Control de Maquinas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Sistemas Electricos de Potencia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Calidad de la Energia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Topicos de Ingenieria Electrica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electrica",
          "Materia": "Seminario de Titulacion de Ingenieria Electrica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Calculo I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Fisica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Quimica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "algebra"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Introduccion a la Ingenieria Mecanica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Dibujo Mecanico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Calculo II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Fisica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Analisis Numerico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Ingenieria en Manufactura"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Probabilidad y Estadistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Programacion Estructurada"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Calculo III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Fisica III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Ecuaciones Diferenciales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Ciencias de los Materiales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Competencias Comunicativas con Enfoque de Genero"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Ingles Comunicativo Principiante"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Circuitos Electricos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Termodinamica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Mecanica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Sistemas Dinamicos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Mecanica de Fluidos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Electronica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Sistemas de Combustion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Mecanica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Transferencia de Calor"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Termofluidos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Competencias para el Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Hidrodinamica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Metrologia e Instrumentacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Dise�o Mecanico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Ingles Comunicativo Intermedio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Control I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Dise�o e Innovacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Procesos de Manufactura I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Dise�o Mecanico II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Ingenieria Economica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Mecanismos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Desarrollo Empresarial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Ingenieria� en Sistemas Mecanicos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Vibraciones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Competencias en el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Robotica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Seguridad Industrial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Proyecto de Titulacion Mecanico I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Metodo del Elemento Finito"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Aire Acondicionado y Refrigeracion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Mecanica",
          "Materia": "Ingles Comunicativo Avanzado"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Competencias Comun?icativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Anatomia y Disecciones Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Fisiologia Celular y Biofisica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Exterior y Manejo de los Animales Domesticos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Estadistica Descriptiva"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Bioetica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Anatomia Topografica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Fisiologia Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Histologia y Embriologia Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Etologia Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Bioquimica Veterinaria I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Estadistica Inferencial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Virologia y Enfermedades Virales de los Animales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Propedeutica Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Bacteriologia y Micologia Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Bioquimica Veterinaria II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Ecologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Dise�o Experimental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Competencias para el Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Metodologia de la Investigacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Inmunologia Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Parasitologia y Enfermedades Parasitarias I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Farmacologia Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Genetica y Mejoramiento Animal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Nutricion Animal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Parasitologia y Enfermedades Parasitarias II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Alimentacion Animal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Patologia General Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Reproduccion Animal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Tecnicas y Terapeuticas Quirurgicas Veterinarias I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Patologia Sistemica Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Economia y Administracion Pecuaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Toxicologia Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Calidad e Inocuidad Alimentaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Seminario de Tesis"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Tecnicas y Terapeutica Quirurgica Veterinaria II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Imagenologia Diagnostica Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Epidemiologia Veterinaria?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Patologia Clinica Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Manejo y Aprovechamiento de Recursos Naturales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Zootecnia y Medicina de Bovinos Productores de Leche??"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Zootecnia� y Medicina de Bovinos Productores de Carne"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Zootecnia y Medicina de Porcinos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Salud Publica Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Marco Legal de la Medicina Veterinaria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Zootecnia de Perros y Gatos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Medicina de Perros y Gatos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Zootecnia y Medicina de Equinos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Zootecnia y Medicina de Ovinos y Caprinos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Medico Veterinario Zootecnista",
          "Materia": "Zootecnia y Medicina de Aves"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Fundamentos Basicos de Admon."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Matematicas Basicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Estadistica Descriptiva"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Contabilidad Financiera I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Ingles Comunicativo� Basico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Introduccion al Estudio del Derecho"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Fundamentos de Teoria Economica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Contabilidad Financiera II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Compt.� Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Ingles Comunicativo Intermedio"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Introduccion a las Finanzas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Derecho Mercantil I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Microeconomia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Contabilidad de Costos I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Ingles Comunicativo Avanz."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Finanzas I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Derecho Laboral"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Macroeconomia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Contabilidad Intermedia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Contabilidad de Costos II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Finanzas II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Derecho Fiscal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Contabilidad de Sociedades"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Compt. Desarrollo Humano"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Introduccion a la Auditoria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Finanzas III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Impuestos� I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Prevision y Seguridad Social"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Practicas de Auditoria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Form. y Eval. De Proy. Inv."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Impuestos II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Contabilidad Financiera III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Organizacion Contable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Dictamenes de Auditoria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Creacion y Des.Emp."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Impuestos III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Seminario de investigacion I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Contabilidad Financiera IV"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Compt. p/el ejercicio de la Ciud."
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Seminario de investigacion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Finanzas Internacionales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Contabilidad Internacional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Estrategias gerenciales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Contaduria",
          "Materia": "Seminario de Finanzas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Quimica General?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Quimica Inorganica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Quimica Organica I??"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Matematicas I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Quimica Organica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Biologia Celular"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Metodologia de la Investigacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Estadistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Matematicas II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Analisis Quimico I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Fisicoquimica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Fisicoquimica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Algebra Lineal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Competencias Comunicativas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Ingles Comunicativo (Principiante)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Ingles Comunicativo (Intermedio)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Fisica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Analisis Quimico II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Microbiologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Investigacion I??"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Quimica Computacional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Bioquimica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Anatomia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Bioquimica I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Inmunologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Bioquimica Clinica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Microbiologia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Farmacia I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Farmacia II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Fisiologia General"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Micologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Epidemiologia General"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Microbiologia III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Competencias Para El Desarrollo Humano Sustentable"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Ingles Comunicativo (Avanzado)"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Quimica Organica III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Dise�o Experimental"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Investigacion II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Investigacion III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Inmunoquimica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Hematologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Analisis Quimicos III?"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Biologia Molecular"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Patologia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Control De Calidad"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Farmacologia Clinica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Fisicoquimica Farmaceutica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Tecnologia Farmaceutica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Desarrollo Farmaceutico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Tecnologia Farmaceutica II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Farmacia III"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Quimico Farmaceutico Biologo",
          "Materia": "Competencias para el Ejercicio de la Ciudadania"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Introduccion a la Ingenieria"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Arquitectura de Computadores"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Se�ales y Sistemas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Dispositivos Electronicos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Teoria de Circuitos�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Fisica Moderna�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Control Automatico�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Procesos estocasticos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Circuitos Electronicos I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Circuitos Electronicos II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Laboratorio de Electronica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Electromagnetismo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Conduccion e irradiacion de ondas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Procesamiento de Se�ales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Teoria de la informacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Circuitos para Radiofrecuencias"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Circuitos Digitales"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Sistemas de Comunicaciones Digitales y Analogicas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Laboratorio de Comunicaciones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Dispositivos de Comunicaciones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Laboratorio de Microprocesadores"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Probabilidad y Estadistica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "algebra"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "algebra Lineal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Quimica General"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Sistemas de Representacion para Electronica"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Introduccion a la Programacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Introduccion a las Telecomunicaciones"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Sistemas Digitales I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Sistemas Digitales II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Matematica Avanzada"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Ingenieria Legal"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Fisica I�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Fisica II a�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Fisica II b�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Calculo Numerico"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Fundamentos de Economia y Finanzas�"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Organizacion, Gestion y Entrepreneurship"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Analisis Matematico I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Analisis Matematico II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Analisis Matematico III a"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Analisis Matematico III b"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Formulacion de Proyectos y del Trabajo Final"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ingenieria Electronica",
          "Materia": "Seguridad ambiental y del Trabajo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Filosofia de la educacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Psicologia del desarrollo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Didactica general"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Paqueteria computacional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Sociologia de la educacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Psicologia del ni�o, del adolescente y del adulto"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Tecnologias de la informacion y la comunicacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Epistemologia de la educacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Temas selectos de pedagogia"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Planeacion y evaluacion del aprendizaje"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Habilidades del pensamiento critico y creativo"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Planeacion y dise�o de propuestas didacticas"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Metodologia y tecnicas de investigacion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Psicologia diferencial"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Psicoling�istica: teorias del aprendizaje"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Gestion educativa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "etica y desarrollo profesional"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Metodos y tecnicas de la ense�anza del ingles"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Dise�o y evaluacion de materiales para la ense�anza del idioma ingles"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Habilidades comunicativas en ingles I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Fonetica y fonologia para la ense�anza del idioma ingles I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Fonetica y fonologia para la ense�anza del idioma ingles II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Habilidades comunicativas en ingles II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Morfologia y semantica para la ense�anza del idioma ingles"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Fonetica, fonologia y morfologia del espa�ol"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Analisis gramatical del idioma ingles"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Analisis del discurso"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Gramatica del espa�ol"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Didactica del idioma ingles"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Gramatica comparativa del idioma ingles y espa�ol"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Expresion oral en ingles"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Cultura y literatura norteamericana"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Estilos de aprendizaje I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Psicopedagogia para el aprendizaje del ingles"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Cultura y literatura inglesa"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Estilos de aprendizaje II"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Desarrollo de habilidades de escritura y lectura en ingles"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Observacion de la practica docente"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Ling�istica aplicada"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Seminario de ayudantia y practica docente"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Tecnicas de traduccion"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Seminario de investigacion en la ense�anza del ingles"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Tecnicas de interpretacion simultanea y consecutiva"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Ense�anza de sistemas ling�isticos"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Taller de comunicacion oral y auditiva en ingles I"
        },
        {
          "Nivel": "Universidad",
          "Categoria": "Licenciatura en Ense�anza del Ingles",
          "Materia": "Taller de comunicacion oral y auditiva en ingles II"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Comunicacion",
          "Materia": "Taller de lectura y redaccion I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Comunicacion",
          "Materia": "Taller de lectura y redaccion I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Comunicacion",
          "Materia": "Informatica I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Comunicacion",
          "Materia": "Informatica II"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Matematicas",
          "Materia": "Matematicas I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Matematicas",
          "Materia": "Matematicas II"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Matematicas",
          "Materia": "Matematicas III"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Matematicas",
          "Materia": "Matematicas IV"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Humanidades",
          "Materia": "etica y Valores I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Humanidades",
          "Materia": "etica y Valores II"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Humanidades",
          "Materia": "Literatura I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Humanidades",
          "Materia": "Literatura II"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Humanidades",
          "Materia": "Filosofia"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias experimentales",
          "Materia": "Quimica I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias experimentales",
          "Materia": "Quimica II"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias experimentales",
          "Materia": "Biologia I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias experimentales",
          "Materia": "Biologia II"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias experimentales",
          "Materia": "Fisica I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias experimentales",
          "Materia": "Fisica II"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias experimentales",
          "Materia": "Geografia"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias experimentales",
          "Materia": "Ecologia y Medio Ambiente"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias sociales",
          "Materia": "Metodologia de la Investigacion"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias sociales",
          "Materia": "Introduccion a las Ciencias Sociales"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias sociales",
          "Materia": "Historia de Mexico I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias sociales",
          "Materia": "Historia de Mexico II"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias sociales",
          "Materia": "Estructura socioeconomica de Mexico"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ciencias sociales",
          "Materia": "Historia Universal Contemporanea"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ingles",
          "Materia": "Ingles I"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ingles",
          "Materia": "Ingles II"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ingles",
          "Materia": "Ingles III"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ingles",
          "Materia": "Ingles VI"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ingles",
          "Materia": "Ingles V"
        },
        {
          "Nivel": "Bachillerato general",
          "Categoria": "Ingles",
          "Materia": "Ingles VI"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Comunicacion",
          "Materia": "Tecnologia de la Informacion y la comunicacion"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Comunicacion",
          "Materia": "Lectura, expresion oral y escrita I"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Comunicacion",
          "Materia": "Lectura, expresion oral y escrita II"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Matematicas",
          "Materia": "algebra"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Matematicas",
          "Materia": "Geometria y Trigonometria"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Matematicas",
          "Materia": "Geometria analitica"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Matematicas",
          "Materia": "Calculo diferencial"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Matematicas",
          "Materia": "Calculo integral"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Matematicas",
          "Materia": "Probabilidad y estadistica"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Humanidades",
          "Materia": "Logica"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Humanidades",
          "Materia": "etica"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Humanidades",
          "Materia": "Temas de Filosofia"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ciencias experimentales",
          "Materia": "Quimica I"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ciencias experimentales",
          "Materia": "Quimica II"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ciencias experimentales",
          "Materia": "Biologia I"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ciencias experimentales",
          "Materia": "Fisica I"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ciencias experimentales",
          "Materia": "Fisica II"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ciencias experimentales",
          "Materia": "Ecologia"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ciencias sociales",
          "Materia": "Ciencia"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ciencias sociales",
          "Materia": "Tecnologia"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ciencias sociales",
          "Materia": "Sociedad"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ciencias sociales",
          "Materia": "Valores"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ingles",
          "Materia": "Ingles I"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ingles",
          "Materia": "Ingles II"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ingles",
          "Materia": "Ingles III"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ingles",
          "Materia": "Ingles VI"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ingles",
          "Materia": "Ingles V"
        },
        {
          "Nivel": "Bachillerato tecnologico",
          "Categoria": "Ingles",
          "Materia": "Ingles VI"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Primer grado",
          "Materia": "Espa�ol I"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Primer grado",
          "Materia": "Matematicas I"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Primer grado",
          "Materia": "Ciencias I (Biologia)"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Primer grado",
          "Materia": "Geografia de Mexico y del Mundo"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Primer grado",
          "Materia": "Ingles I"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Primer grado",
          "Materia": "Computacion"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Segundo grado",
          "Materia": "Espa�ol II"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Segundo grado",
          "Materia": "Matematicas II"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Segundo grado",
          "Materia": "Ciencias II (Fisica)"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Segundo grado",
          "Materia": "Historia I Universal"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Segundo grado",
          "Materia": "Ingles II"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Segundo grado",
          "Materia": "Computacion"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Tercer grado",
          "Materia": "Espa�ol III"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Tercer grado",
          "Materia": "Matematicas III"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Tercer grado",
          "Materia": "Ciencias III (Quimica)"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Tercer grado",
          "Materia": "HistoriaII de Mexico"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Tercer grado",
          "Materia": "Ingles III"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Tercer grado",
          "Materia": "Computacion"
        },
        {
          "Nivel": "Secundaria",
          "Categoria": "Tercer grado",
          "Materia": "Frances III"
        },
        {
          "Nivel": "Primaria",
          "Categoria": "Basico",
          "Materia": "Geografia"
        },
        {
          "Nivel": "Primaria",
          "Categoria": "Basico",
          "Materia": "Ciencias Naturales"
        },
        {
          "Nivel": "Primaria",
          "Categoria": "Basico",
          "Materia": "Espa�ol"
        },
        {
          "Nivel": "Primaria",
          "Categoria": "Basico",
          "Materia": "Literatura"
        },
        {
          "Nivel": "Primaria",
          "Categoria": "Basico",
          "Materia": "Matematicas"
        },
        {
          "Nivel": "Primaria",
          "Categoria": "Basico",
          "Materia": "Ingl�s"
        },
        {
          "Nivel": "Primaria",
          "Categoria": "Basico",
          "Materia": "Exploraci�n de la Naturaleza y la Sociedad"
        }

];