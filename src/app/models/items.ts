export interface Item {
  nombre? : string;
  apellidoPaterno? : string;
  apellidoMaterno? : string;
  correo? : string;
  calle? : string;
  numero? : string;
  colonia? : string;
  cp? : string;
  estado? : string;
  fechaNacimiento? : string;
  telefono? : string;
  ocupacion? : string;
  }