
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of} from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

export interface Usuarios{
  nombre : string;
  email : string;
  apaterno : string;
  fechanac : string;
}

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':  'application/json'})
};

@Injectable()
export class RestService {
  private apiURL = 'http://68.183.164.56:3000/usuario/';

  constructor(private http: HttpClient) { }

   /** POST: añadimos un nuevo usuario */
   addUser (usuario: Usuarios): Observable<Usuarios> {
    return this.http.post<Usuarios>(this.apiURL, usuario, httpOptions).pipe(
      tap((usuario: Usuarios) => this.log(`added user w/ id=${usuario}`)),
      catchError(this.handleError<Usuarios>('addUser'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {

  }

}