import { TestBed } from '@angular/core/testing';

import { TichfsService } from './tichfs.service';

describe('TichfsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TichfsService = TestBed.get(TichfsService);
    expect(service).toBeTruthy();
  });
});
