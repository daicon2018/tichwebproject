import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Item } from '../models/items';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TichfsService {
  itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;

  constructor(public afs: AngularFirestore) { 
    this.items = this.afs.collection('items').valueChanges();
//    this.items = this.afs.collection('items').snapshotChanges().map(changes => {
//      return changes.map(a => {
//        const data = a.payload.doc.data() as Item;
//        data.
//      })
//    });
  }

  getItems(){
    return this.items;
  }

}

